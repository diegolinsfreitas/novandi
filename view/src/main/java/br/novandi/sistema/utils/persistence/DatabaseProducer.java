package br.novandi.sistema.utils.persistence;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class DatabaseProducer {

	/*
	 * @Produces
	 * 
	 * @PersistenceContext(unitName = "application-pu")
	 * 
	 * @ApplicationDatabase private EntityManager em;
	 */
	@Produces
	@PersistenceContext(unitName = "application-pu")
	private EntityManager em;

	/*@Produces
	public EntityManager createEntityManager(EntityManagerFactory entityManagerFactory) {
		return entityManagerFactory.createEntityManager();
	}

	public void close(@Disposes EntityManager entityManager) {
		entityManager.close();
	}*/


}
